module me.max.jfx.jfxsettings {
    requires javafx.controls;
    requires javafx.fxml;

    requires org.controlsfx.controls;

    exports me.max.jfx.jfxsettings.model;
    exports me.max.jfx.jfxsettings.base.variable;
    exports me.max.jfx.jfxsettings.app;
}