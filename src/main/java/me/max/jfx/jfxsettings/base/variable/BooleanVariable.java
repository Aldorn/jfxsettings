package me.max.jfx.jfxsettings.base.variable;

import javafx.beans.property.SimpleBooleanProperty;

public final class BooleanVariable extends SimpleBooleanProperty implements Variable {
    private final String name;

    public BooleanVariable(boolean initialValue) {
        this(initialValue, "");
    }

    public BooleanVariable(boolean initialValue, String name) {
        super(initialValue);
        this.name = name;
    }

    @Override
    public void visit(PropertyVisitor visitor) {
        visitor.visitBoolean(this);
    }

    @Override
    public String getName() {
        return name;
    }
}
