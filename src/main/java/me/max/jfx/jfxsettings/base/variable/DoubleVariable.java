package me.max.jfx.jfxsettings.base.variable;

import javafx.beans.property.SimpleDoubleProperty;

public final class DoubleVariable extends SimpleDoubleProperty implements Variable {
    private String name;

    public DoubleVariable(double initialValue) {
        this(initialValue, "");
    }

    public DoubleVariable(double initialValue, String name) {
        super(initialValue);
        this.name = name;
    }
    @Override
    public void visit(PropertyVisitor visitor) {
        visitor.visitDouble(this);
    }
}
