package me.max.jfx.jfxsettings.base.variable;

public sealed interface ReadOnlyVariable permits ReadOnlyChoiceVariable, Variable {
    String getName();

    void visit(ReadOnlyVisitor visitor);
}
