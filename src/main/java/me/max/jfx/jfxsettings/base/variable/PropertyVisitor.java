package me.max.jfx.jfxsettings.base.variable;

import javafx.beans.property.*;

public interface PropertyVisitor {
    void visitInt(IntegerProperty var);
    void visitString(StringProperty var);
    void visitBoolean(BooleanProperty var);
    void visitDouble(DoubleProperty var);
    void visitChoice(ChoiceVariable<?> var);
}
