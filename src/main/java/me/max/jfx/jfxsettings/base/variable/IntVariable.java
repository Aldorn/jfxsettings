package me.max.jfx.jfxsettings.base.variable;

import javafx.beans.property.SimpleIntegerProperty;

public final class IntVariable extends SimpleIntegerProperty implements Variable {
    private final String name;

    public IntVariable(int initialValue) {
        super(initialValue);
        name = "";
    }

    public IntVariable(int initialValue, String name) {
        super(initialValue);
        this.name = name;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public void visit(PropertyVisitor visitor) {
        visitor.visitInt(this);
    }
}
