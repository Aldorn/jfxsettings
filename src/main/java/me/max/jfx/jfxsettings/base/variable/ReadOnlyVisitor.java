package me.max.jfx.jfxsettings.base.variable;

import javafx.beans.property.*;

public interface ReadOnlyVisitor extends PropertyVisitor {
    void visitInt(ReadOnlyIntegerProperty var);

    void visitString(ReadOnlyStringProperty var);

    void visitBoolean(ReadOnlyBooleanProperty var);

    void visitDouble(ReadOnlyDoubleProperty var);

    void visitChoice(ReadOnlyChoiceVariable<?> tChoiceVariable);

    @Override
    default void visitInt(IntegerProperty var) {
        visitInt((ReadOnlyIntegerProperty) var);
    }

    @Override
    default void visitString(StringProperty var) {
        visitString((ReadOnlyStringProperty) var);
    }

    @Override
    default void visitBoolean(BooleanProperty var) {
        visitBoolean((ReadOnlyBooleanProperty) var);
    }

    @Override
    default void visitDouble(DoubleProperty var) {
        visitDouble((ReadOnlyDoubleProperty) var);
    }

    @Override
    default void visitChoice(ChoiceVariable<?> var) {
        visitChoice((ReadOnlyChoiceVariable<?>) var);
    }

}
