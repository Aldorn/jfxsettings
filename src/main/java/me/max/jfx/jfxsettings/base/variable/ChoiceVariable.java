package me.max.jfx.jfxsettings.base.variable;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.collections.ObservableList;

public final class ChoiceVariable<T> implements Variable, ReadOnlyChoiceVariable<T> {
    private final ObservableList<T> values;
    private final IntegerProperty selected;
    private final String name;

    public ChoiceVariable(ObservableList<T> values, int selected) {
        this(values, selected, "");
    }
    public ChoiceVariable(ObservableList<T> values, int selected, String name) {
        this.values = values;
        this.name = name;
        this.selected = new SimpleIntegerProperty(selected);
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void visit(PropertyVisitor visitor) {
        visitor.visitChoice(this);
    }

    @Override
    public ObservableList<T> getValues() {
        return values;
    }

    @Override
    public int getSelected() {
        return selected.get();
    }

    @Override
    public IntegerProperty selectedProperty() {
        return selected;
    }

    public void select(int index) {
        selected.set(index);
    }
}
