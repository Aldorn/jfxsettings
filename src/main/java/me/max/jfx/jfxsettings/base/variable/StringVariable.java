package me.max.jfx.jfxsettings.base.variable;

import javafx.beans.property.SimpleStringProperty;

public final class StringVariable extends SimpleStringProperty implements Variable {
    private final String name;

    public StringVariable(String initialValue) {
        super(initialValue);
        name = "";
    }

    public StringVariable(String initialValue, String name) {
        super(initialValue);
        this.name = name;
    }

    @Override
    public void visit(PropertyVisitor visitor) {
        visitor.visitString(this);
    }

    @Override
    public String getName() {
        return name;
    }
}
