package me.max.jfx.jfxsettings.base.variable;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ReadOnlyIntegerProperty;
import javafx.collections.ObservableList;

// TODO
public sealed interface ReadOnlyChoiceVariable<T> extends ReadOnlyVariable permits ChoiceVariable {
    ObservableList<T> getValues();

    int getSelected();

    ReadOnlyIntegerProperty selectedProperty();
}
