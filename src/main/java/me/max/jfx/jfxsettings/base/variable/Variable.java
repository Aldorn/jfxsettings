package me.max.jfx.jfxsettings.base.variable;

public sealed interface Variable extends ReadOnlyVariable permits BooleanVariable, ChoiceVariable, DoubleVariable, IntVariable, StringVariable {
    void visit(PropertyVisitor visitor);

    @Override
    default void visit(ReadOnlyVisitor visitor) {
        visit((PropertyVisitor) visitor);
    }
}
