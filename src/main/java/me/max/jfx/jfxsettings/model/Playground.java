package me.max.jfx.jfxsettings.model;

import me.max.jfx.jfxsettings.base.variable.*;

import java.util.Collection;

public interface Playground {
    BooleanVariable isOpen();
    String name();
    Collection<Variable> settings();
    Collection<ReadOnlyVariable> data();
}
