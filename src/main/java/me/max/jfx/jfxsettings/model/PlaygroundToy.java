package me.max.jfx.jfxsettings.model;

public enum PlaygroundToy {
    SEESAW("See Saw"),
    SLIDE("Slide"),
    SWING("Swing"),
    SANDBOX("Sandbox"),
    PLAYHOUSE("Playhouse")
    ;
    private final String name;

    PlaygroundToy(String name) {
        this.name = name;
    }
}
