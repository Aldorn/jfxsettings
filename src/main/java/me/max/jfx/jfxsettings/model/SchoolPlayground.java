package me.max.jfx.jfxsettings.model;

import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.beans.value.ObservableBooleanValue;
import javafx.util.Duration;
import me.max.jfx.jfxsettings.base.variable.BooleanVariable;
import me.max.jfx.jfxsettings.base.variable.IntVariable;
import me.max.jfx.jfxsettings.base.variable.ReadOnlyVariable;
import me.max.jfx.jfxsettings.base.variable.Variable;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class SchoolPlayground implements Playground {
    private BooleanVariable open;
    private IntVariable nextBreak;
    private IntVariable numChildren;
    private IntVariable numTeachers;
    private BooleanVariable skipClasses;

    public SchoolPlayground() {
        open = new BooleanVariable(false, "open");
        open.addListener(b -> System.out.println("playground " + b));
        nextBreak = new IntVariable(0, "next break");
        numChildren = new IntVariable(0, "num children");
        numTeachers = new IntVariable(0, "num teachers");
        skipClasses = new BooleanVariable(false, "skip classes");

        initTimeline();
    }

    @Override
    public BooleanVariable isOpen() {
        return open;
    }

    @Override
    public String name() {
        return "School Playground";
    }

    @Override
    public Collection<Variable> settings() {
        return List.of(skipClasses);
    }

    @Override
    public Collection<ReadOnlyVariable> data() {
        return List.of(numChildren, numTeachers, nextBreak);
    }

    private void initTimeline() {
        Timeline player = new Timeline(new KeyFrame(Duration.millis(100), e -> assignRandomValues()));
        player.setCycleCount(Animation.INDEFINITE);
        open.addListener((observable, oldv, newV) -> {
            System.out.println(observable);
            if (((ObservableBooleanValue) observable).get()) {
                player.play();
            } else {
                player.stop();
            }
        });
    }

    private void assignRandomValues() {
        Random random = new Random();
        if (nextBreak.get() > 0 && !skipClasses.get()) {
            nextBreak.set(nextBreak.get() - 1);
            if (numChildren.get() > 0 && random.nextInt(100) > 50) {
                numChildren.set(numChildren.get() - 1);
            }
            if (numTeachers.get() > 0 && random.nextInt(100) > 30) {
                numTeachers.set(numTeachers.get() - 1);
            }
        } else {
            if (random.nextInt(100) < 3 && !skipClasses.get()) {
                nextBreak.set(random.nextInt(200, 500));
            } else {
                if (random.nextInt(100) < 40)
                    numChildren.set(numChildren.get() + random.nextInt(5));
                if (random.nextInt(100) < 30)
                    numTeachers.set(numTeachers.get() + random.nextInt(2));
            }
        }
    }
}
