package me.max.jfx.jfxsettings.model;

import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.beans.property.SimpleListProperty;
import javafx.beans.value.ObservableBooleanValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.util.Duration;
import me.max.jfx.jfxsettings.base.variable.*;

import java.util.Collection;
import java.util.List;
import java.util.Random;

public class PublicPlayground implements Playground {
    private BooleanVariable open;
    private ChoiceVariable<PlaygroundToy> toy;
    private BooleanVariable allowDogs;
    private IntVariable numParents;

    public PublicPlayground() {
        open = new BooleanVariable(false, "open");
        toy = new ChoiceVariable<>(FXCollections.observableArrayList(PlaygroundToy.values()), 0, "toy");
        allowDogs = new BooleanVariable(true, "allow dogs");
        numParents = new IntVariable(0, "number of parents");

        initTimeline();
    }

    @Override
    public BooleanVariable isOpen() {
        return open;
    }

    @Override
    public String name() {
        return "playground";
    }

    @Override
    public Collection<Variable> settings() {
        return List.of(toy, allowDogs);
    }

    @Override
    public Collection<ReadOnlyVariable> data() {
        return List.of(numParents);
    }

    private void initTimeline() {
        Timeline player = new Timeline(new KeyFrame(Duration.millis(1000), e -> assignRandomValues()));
        player.setCycleCount(Animation.INDEFINITE);
        open.addListener((observable, oldv, newV) -> {
            System.out.println(observable);
            if (((ObservableBooleanValue) observable).get()) {
                player.playFromStart();
            } else {
                player.stop();
            }
        });
    }

    private void assignRandomValues() {
        Random random = new Random();
        numParents.set(Math.max(0, numParents.get() + random.nextInt(-5, 5)));
    }
}
