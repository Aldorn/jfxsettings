package me.max.jfx.jfxsettings.jfx;

import javafx.beans.property.BooleanProperty;
import javafx.scene.Node;
import javafx.scene.control.CheckBox;
import me.max.jfx.jfxsettings.base.variable.BooleanVariable;
import me.max.jfx.jfxsettings.jfx.visitor.DisplayElementFactory;
import me.max.jfx.jfxsettings.jfx.visitor.SettingElementFactory;
import me.max.jfx.jfxsettings.model.Playground;

import java.util.List;
import java.util.stream.Stream;

public class PlaygroundDisplayFactory {
    private Playground playground;

    public PlaygroundDisplayFactory(Playground playground) {
        this.playground = playground;
    }

    public CheckBox enable() {
        var box = new CheckBox(playground.name());
        BooleanProperty prop = playground.isOpen();
        box.selectedProperty().bindBidirectional(prop);
        return box;
    }

    public List<Node> settings() {
        return playground.settings().stream().map(SettingElementFactory::getSettingNodeFor).toList();
    }

    public List<Node> data() {
        return Stream.concat(playground.data().stream(), playground.settings().stream()).map(DisplayElementFactory::getDisplayNodeFor).toList();
    }
}
