package me.max.jfx.jfxsettings.jfx.visitor;

import javafx.beans.binding.Bindings;
import javafx.beans.property.*;
import javafx.geometry.Orientation;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import me.max.jfx.jfxsettings.base.variable.*;

import java.util.Objects;

public class DisplayElementFactory implements ReadOnlyVisitor {
    private Node node;

    public static Node getDisplayNodeFor(ReadOnlyVariable var) {
        Objects.requireNonNull(var);
        var visitor = new DisplayElementFactory();
        var.visit(visitor);
        Objects.requireNonNull(visitor.node);
        var node = visitor.node;
        var label = new Label(var.getName());
        var labeled = new HBox(10.0, node, label);
        return labeled;
    }

    private DisplayElementFactory() { }

    @Override
    public void visitInt(ReadOnlyIntegerProperty var) {
        var text = new TextField("" + var.intValue());
        text.textProperty().bind(var.asString());
        node = text;
    }

    @Override
    public void visitString(ReadOnlyStringProperty var) {
        var text = new TextField(var.getValue());
        text.textProperty().bind(var);
        node = text;
    }

    @Override
    public void visitBoolean(ReadOnlyBooleanProperty var) {
        var text = new TextField("" + var.getValue());
        text.textProperty().bind(var.asString());
        node = text;
    }

    @Override
    public void visitDouble(ReadOnlyDoubleProperty var) {
        var text = new TextField("" + var.doubleValue());
        text.textProperty().bind(var.asString());
        node = text;
    }

    @Override
    public void visitChoice(ReadOnlyChoiceVariable<?> choice) {
        var text = new TextField(choice.getValues().get(choice.getSelected()).toString());
        var binding = Bindings.valueAt(choice.getValues(), choice.selectedProperty());
        text.textProperty().bind(binding.asString());
        node = text;
    }
}
