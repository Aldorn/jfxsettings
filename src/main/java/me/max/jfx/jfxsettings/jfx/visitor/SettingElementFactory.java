package me.max.jfx.jfxsettings.jfx.visitor;

import javafx.beans.binding.Bindings;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.StringProperty;
import javafx.scene.Node;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Spinner;
import javafx.scene.control.TextField;
import me.max.jfx.jfxsettings.base.variable.ChoiceVariable;
import me.max.jfx.jfxsettings.base.variable.PropertyVisitor;
import me.max.jfx.jfxsettings.base.variable.Variable;

import java.util.Objects;

public class SettingElementFactory implements PropertyVisitor {
    private Node node;

    public static Node getSettingNodeFor(Variable var) {
        var visitor = new SettingElementFactory();
        var.visit(visitor);
        Objects.requireNonNull(visitor.node);
        return visitor.node;
    }
    private SettingElementFactory() { }
    @Override
    public void visitInt(IntegerProperty var) {
        var spinner = new Spinner<Integer>(Integer.MIN_VALUE, Integer.MAX_VALUE, var.intValue());
        spinner.getValueFactory().valueProperty().bindBidirectional(var.asObject());
        node = spinner;
    }

    @Override
    public void visitString(StringProperty var) {
        var edit = new TextField(var.getValue());
        edit.textProperty().bindBidirectional(var);
        node = edit;
    }

    @Override
    public void visitBoolean(BooleanProperty var) {
        var box = new CheckBox(var.getName());
        box.selectedProperty().bindBidirectional(var);
        node = box;
    }

    @Override
    public void visitDouble(DoubleProperty var) {
        var spinner = new Spinner<Double>(Double.MIN_VALUE, Double.MAX_VALUE, var.doubleValue());
        spinner.getValueFactory().valueProperty().bindBidirectional(var.asObject());
        node = spinner;
    }

    @Override
    public void visitChoice(ChoiceVariable<?> choice) {
        var combo = new ComboBox<>(choice.getValues());
        choice.selectedProperty().addListener((observable, oldV, newV) -> {
            if (newV.intValue() != combo.getSelectionModel().getSelectedIndex()) {
                combo.getSelectionModel().select(newV.intValue());
            }
        });
        combo.getSelectionModel().selectedIndexProperty().addListener((observable, oldV, newV) -> {
            if (newV.intValue() != choice.getSelected()) {
                choice.select(newV.intValue());
            }
        });
        combo.getSelectionModel().select(choice.getSelected());
        node = combo;
    }
}
