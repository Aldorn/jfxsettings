package me.max.jfx.jfxsettings.app;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Orientation;
import javafx.scene.Scene;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.TilePane;
import javafx.stage.Stage;
import me.max.jfx.jfxsettings.jfx.PlaygroundDisplayFactory;
import me.max.jfx.jfxsettings.jfx.visitor.SettingElementFactory;
import me.max.jfx.jfxsettings.model.Playground;
import me.max.jfx.jfxsettings.model.PublicPlayground;
import me.max.jfx.jfxsettings.model.SchoolPlayground;

import java.io.IOException;
import java.util.List;

public class HelloApplication extends Application {
    private List<Playground> playgrounds;

    public HelloApplication() {
        super();
        playgrounds = List.of(new SchoolPlayground(), new PublicPlayground());
    }
    @Override
    public void start(Stage stage) throws IOException {
        TilePane enable = new TilePane(Orientation.VERTICAL);
        TilePane settings = new TilePane(Orientation.VERTICAL);
        TilePane data = new TilePane(Orientation.VERTICAL);

        for (var p: playgrounds) {
            var factory = new PlaygroundDisplayFactory(p);
            enable.getChildren().add(factory.enable());
            settings.getChildren().addAll(factory.settings());
            data.getChildren().addAll(factory.data());
        }

        GridPane content = new GridPane();
        content.add(new ScrollPane(enable), 0, 0);
        content.add(new ScrollPane(settings), 1, 0);
        content.add(new ScrollPane(data), 2, 0);

        Scene scene = new Scene(content);
        stage.setScene(scene);
        stage.show();
    }

    public static void main(String[] args) {
        launch();
    }
}